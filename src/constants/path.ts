export const HOME = "/";
export const CURRENCIES = "/currencies";
export const ANALYSIS = "/analysis";
export const ANALYSIS_URL = "/analysis/:url";