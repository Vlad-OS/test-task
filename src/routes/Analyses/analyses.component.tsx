
import React,{useEffect, useState} from "react";
import { mapDOM } from "../../utils/mapDom"
import "./styles.css"

export const Analysis: React.FunctionComponent = (props) => {
    const [url, setUrl] = useState('https://www.google.com/');
    const [loading, setLoading] = useState(false);
    const [uniqueTagsState, setUniqueTags] = useState([]);    
    const [commonTagState, setCommonTag] = useState("");
    const [chain, setChains] = useState([]);

    const getAnalysis = async () => {
        setLoading(true)
        const proxyurl = "https://cors-anywhere.herokuapp.com/";
        console.log('url', url)
        const html = (await (await fetch(proxyurl + url)).text()); // html as text
        const doc = new DOMParser().parseFromString(html, 'text/html');//parse html
        const body: any = mapDOM(doc.body, true)// html body to json
        const json = JSON.parse(body)

        const uniqueTags: any = []

        const commonTags: any = {}

        const listOfChains: string[][] = []
        
        let commonTag: any

        const iterateParsedDom = (obj: any, deepCounter: number, chain: string[]) => {//recursive function for getting all chains, unique tags and the most common tag at once
            if (obj.type) {
                if (uniqueTags.indexOf(obj.type) === -1) {
                    console.log('obj.type && uniqueTags.indexOf', obj.type)
                    uniqueTags.push(obj.type)
                }
                if (obj.type && !commonTags[obj.type]) {
                    commonTags[obj.type] = 1
                } else {
                    commonTags[obj.type] =  1 + commonTags[obj.type]
                }
                listOfChains.push(chain)
                obj.content && obj.content.forEach((iterationObj: any) => {
                    iterateParsedDom(iterationObj, deepCounter++, [...chain, obj.type])
                })
            }
        }

        iterateParsedDom(json, 0, [])


        let repeatsTag: number = 0
        Object.keys(commonTags).forEach((key) => { //get the mosts common tag
            if (commonTags[key] > repeatsTag) {
                repeatsTag = commonTags[key]
                commonTag = key
            }
        })
       
        
        const countOccurrences = (arr: string[], val: string) => arr.reduce((a, v) => (v === val ? a + 1 : a), 0);
        let maxCount: number = 0
        let maxLength: number = 0
        let longestTopChains: any = []
        listOfChains.forEach(((value) => {// Find the longest path in the document tree where the most popular tag is used the most times.
            if (countOccurrences(value, commonTag) > maxCount) {
                maxCount = countOccurrences(value, commonTag)
                if (value.length > maxLength) {
                    maxLength = value.length
                    longestTopChains = [[...value]]
                } else if (value.length > maxLength) {
                    longestTopChains.push(value)
                }
            }
        }))
        setUniqueTags(uniqueTags)
        setChains(longestTopChains)
        setCommonTag(commonTag)
        setLoading(false)
    }


    return(
        <div className="container">
            <h1>Analysis</h1>
            <p>Put link in input</p>
            <div className="input-container">
                <input
                    className="analyse-input"
                    name="url"
                    type="text"
                    placeholder="Put link in input"
                    onChange={({target}) => (setUrl(target.value))}
                    value={url}
                />
                <button className="analyse-button" onClick={getAnalysis}>Analysis</button>
            </div>
            {loading && <div>Loading</div>}
            {commonTagState && (
                <React.Fragment>
                    <h6>Most commonly used tag</h6>
                    <p>{commonTagState.toLowerCase()}</p>
                </React.Fragment>
            )}
            {uniqueTagsState.length > 0 &&  (
                <React.Fragment>
                    <h6>List of unique tags</h6>
                    <ul>
                        {uniqueTagsState.map((value:string) => (<li>{value.toLowerCase()}</li>))}
                    </ul>
                    
                </React.Fragment>
            )}
            {chain.length > 0 && (
                <React.Fragment>
                    <h6>Longest path(s) in the document tree where the most popular tag is used the most times</h6>
                    {chain.map((value: string[]) => (
                        <p>{value.join(' > ').toLowerCase()}</p>
                    ))}
                </React.Fragment>
            )}
            
            
        </div>
    )
}