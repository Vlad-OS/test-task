
import React,{useState, useEffect} from "react";
import './style.css'
// https://api.coindesk.com/v1/bpi/currentprice.json

interface Props {
    onRequestCurrencies: () => void;
    onUpdateCurrencies: () => void;
    currencies: {
        chartName: string,
        loading: boolean,
        error: string,
        updateTime: string,
        bpi: {
            
        }
    };
}
  

export const CurrenciesComponent: React.FunctionComponent<Props> = (props) => {

    const [sorting, setSorting] = useState("");
    const [reverseSorting, setReverseSorting] = useState(false);
    const [refreshInterval, setRefreshInterval] = useState(0);

    useEffect(() => {
        if (refreshInterval && refreshInterval > 0) {
          const interval = setInterval(props.onRequestCurrencies, refreshInterval);
          return () => clearInterval(interval);
        }
      }, [refreshInterval]);

    const timeInterval: number = 5000

    useEffect(() => {
        props.onRequestCurrencies()
        let interval = setInterval(() => {//autoupdate
            props.onUpdateCurrencies()
          }, 5000);
      }, []);

    const compare = ( a:any, b:any ) => {
        if ( a[sorting] < b[sorting] ){
            return reverseSorting ? 1 : -1;
        }
        if ( a[sorting] > b[sorting] ){
            return reverseSorting ? -1 : 1;
        }
        return 0;
    }

    const getSortedList = () => {
        const sorted: any[] = []
        Object.values(props.currencies.bpi).map((value) => {
            sorted.push(value)
        })
        
        return sorting ? sorted.sort( compare ) : sorted
        
    }

    const handleSorting = (field: string) => {
        if (sorting !== field) {
            setSorting(field)
        } else {
            setReverseSorting(!reverseSorting)
        }
    }

    const getArrow = (code: string) => {
        if ( sorting === code ) {
            return reverseSorting ? <span>&#8593;</span> : <span>&#8595;</span>;
        } else {
            return null
        }
    }
    

    if (props.currencies.loading) {
        return (<p>Loading</p>)
    } else if (props.currencies.error) {
        return (
            <div>
                <h1>Currencies</h1>
                <h4>{`Error: ${props.currencies.error}`}</h4>
            </div>
        )
    } else {
        return(
            <div className="container">
                <h1>Currencies</h1>

                <h4>{`Updated: ${props.currencies.updateTime}`}</h4>

                <h2>{`Char Name: ${props.currencies.chartName}`}</h2>
                <table>
                    <thead>
                        <tr>
                            <th className="head-sells" onClick={() => handleSorting('code')}>
                                Code {getArrow('code')}
                            </th>
                            <th  className="head-sells" onClick={() => handleSorting('rate')}>
                                Rate {getArrow('rate')}
                            </th>
                            <th  className="head-sells" onClick={() => handleSorting('description')}>
                                Description {getArrow('description')}
                            </th>
                            <th  className="head-sells" onClick={() => handleSorting('rate_float')}>
                                Rate Float {getArrow('rate_float')}
                            </th>
                        </tr>
                    </thead>
                    <tbody>
                    {getSortedList().map((value) => (
                            <tr key={value.code}>
                                <th>{value.code}</th>
                                <th>{value.rate}</th>
                                <th>{value.description}</th>
                                <th>{value.rate_float}</th>
                            </tr>
                        )
                    )}
                    </tbody>
                </table>
            </div>
        )
    }
}