import { connect } from 'react-redux';
import { getCurrenciesAction, updateCurrenciesAction } from '../../store/actions';
import { CurrenciesComponent } from './currencies.component';

const mapStateToProps = (state: any) => ({
    currencies: state.currencies,
});
  

const mapDispatchToProps = (dispatch: any) => ({
    onRequestCurrencies: () => dispatch(getCurrenciesAction()),
    onUpdateCurrencies: () => dispatch(updateCurrenciesAction()),
});

export const CurrenciesContainer = connect(
    mapStateToProps,
    mapDispatchToProps
)(CurrenciesComponent);