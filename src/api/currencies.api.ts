export const getCurrencies = async() => {
  const url = 'https://api.coindesk.com/v1/bpi/currentprice.json';

  return fetch(url).then(data => data.json())
  .then(currentPrice => {
      return currentPrice
  }).catch((error) => {
    throw error
  })
  
};
