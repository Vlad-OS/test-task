import { BaseAction, actionIds } from '../common';

export const getCurrenciesAction = (): BaseAction => ({
  type: actionIds.GET_PRICES_REQUEST,
  payload: null,
});

export const updateCurrenciesAction = (): BaseAction => ({
  type: actionIds.UPDATE_PRICES_REQUEST,
  payload: null,
});

export const getCurrenciesSuccessAction = (data: object): BaseAction => ({
  type: actionIds.GET_PRICES_SUCCESS,
  payload: data,
});


export const numberRequestFailedAction = (data: object): BaseAction => {
  console.log('numberRequestFailedAction', data)
  return ({
    type: actionIds.GET_PRICES_FAILED,
    payload: data,
  })
};
