import { call, put, takeEvery } from 'redux-saga/effects';
import { getCurrencies } from '../../api';
import { getCurrenciesSuccessAction, numberRequestFailedAction } from '../actions';
import { actionIds } from '../common';

export function* watchCurrenciesRequestStart() {
  yield takeEvery(
    actionIds.GET_PRICES_REQUEST,
    requestCurrencies
  );
  yield takeEvery(
    actionIds.UPDATE_PRICES_REQUEST,
    updateCurrencies
  ); 
}

function* requestCurrencies() {
  try {
    const getCurrenciesRequest = yield call(getCurrencies);
    yield put(getCurrenciesSuccessAction(getCurrenciesRequest));
  } catch (e) {
    console.log('e', e)
    yield put(numberRequestFailedAction(e));
  }
}

function* updateCurrencies() {
  try {
    const getCurrenciesRequest = yield call(getCurrencies);
    yield put(getCurrenciesSuccessAction(getCurrenciesRequest));
  } catch (e) {
    console.log('e', e)
    yield put(numberRequestFailedAction(e));
  }
}
