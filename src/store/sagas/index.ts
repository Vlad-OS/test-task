import { all, fork } from 'redux-saga/effects';
import { watchCurrenciesRequestStart } from './currencies.sagas';

export const rootSaga = function* root() {
  yield all([fork(watchCurrenciesRequestStart)]);
};
