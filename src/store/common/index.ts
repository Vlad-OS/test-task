export const actionIds = {
  GET_PRICES_REQUEST: 'Request Currencies',
  UPDATE_PRICES_REQUEST: 'Update Currencies',
  GET_PRICES_SUCCESS: 'Complite Currencies Request',
  GET_PRICES_FAILED: 'Error Currencies Request',
};

export interface BaseAction {
  type: string;
  payload: any;
}
