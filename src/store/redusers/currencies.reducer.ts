import { BaseAction, actionIds } from '../common';

export type CurrenciesCollectionState = {};

export const currenciesCollectionState = (
  state: CurrenciesCollectionState = {loading: true},
  action: BaseAction
) => {
  switch (action.type) {
    case actionIds.GET_PRICES_REQUEST:
      return {
          ...action.payload, 
          loading: true
        };
  case actionIds.UPDATE_PRICES_REQUEST:
      return {
          ...state, 
          loading: false
        };
    case actionIds.GET_PRICES_SUCCESS:
      return {
        ...action.payload,
        updateTime: new Date(Date.now()).toString(),
        loading: false
      };
    case actionIds.GET_PRICES_FAILED:
      console.log('GET_PRICES_FAILED action', action.payload.toString())
      return {
        error: action.payload.toString(),
        loading: false
      };
    };
  return state;
};
