import { combineReducers } from 'redux';
import {
  currenciesCollectionState,
  CurrenciesCollectionState,
} from './currencies.reducer';

export interface State {
  currencies: CurrenciesCollectionState;
}

export const rootReducers = combineReducers<State>({
  currencies: currenciesCollectionState,
});
