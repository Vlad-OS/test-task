import React from "react";
import ReactDOM from "react-dom";
import {
  BrowserRouter as Router,
  Switch,
  Route,
  Link
} from "react-router-dom";
import { Home } from "./routes/Home"
import { CurrenciesContainer } from "./routes/Currencies"
import { Analysis } from "./routes/Analyses"
import { HOME, CURRENCIES, ANALYSIS } from "./constants/path"
import "./assets/styles/styles.css"

export const App: React.FunctionComponent = () => (
  <Router>
  <div>
    <div className="header-wrap">
      <div className="header">
        <nav>
          <Link to={HOME}>Home</Link>
        </nav>
        <nav>
          <Link to={CURRENCIES}>Currencies</Link>
        </nav>
        <nav>
          <Link to={ANALYSIS}>Analysis</Link>
        </nav>
      </div>
    </div>
    <hr />
    <Switch>
      <Route exact path={HOME} component={Home} />
      <Route exact path={CURRENCIES} component={CurrenciesContainer} />
      <Route exact path={ANALYSIS} component={Analysis} />
    </Switch>
  </div>
</Router>
  );